# BatchProcessing

Raccolta di script python3 (python3.7) da eseguire in batch.

**NB:** Segui il branching sviluppo, collaudo, produzione


## Aggiungere nuovo processo
Crea una nuova sottocartella, che contenga almeno:

 - **requirements.txt** : scritto correttamente con anche versioni dei pacchetti
 - **file python da avviare**: con dentro specificato path if name
 - **README.md**: file di readme per indicare cosa fa il codice e le variabili di ambiente necessarie al suo funzionamento
 - **modificare crontab**: aggiungere l'esecuzione nel file crontab nella cartella principale di questo repository


## Esempio file da avviare
File base deve essere
```python
if __name__ == "__main__":
    ...
```

## Logging
Per printare i risultati per debug puoi utilizzare **print** (non consigliato) oppure **logging** usando:

```python
import logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger("Nome applicazione")

logger.info("Esempio")
```

## Aggiunta nel file crontab
Per scegliere l'esecuzione in background del processo è necessario aggiungere una riga al file di crontab, ad esempio:

Specificare sempre:

  - **tempo esecuzione** : vedi https://crontab.guru/ per un aiuto  a scrivere la riga
  - **path python**: sarà sempre /batch/{progetto}/venv/bin/python, sostituendo a {progetto} la cartella del progetto
  - **path file**: sarà sempre /batch/{progetto}/{file} sostituendo a {progetto} il nome della cartella del progetto e {file} il nome del file da eseguire

```
*/30 * * * *	/batch/example/venv/bin/python /batch/example/start.py
0 * * * *	/batch/example2/venv/bin/python /batch/example2/example2.py
```

## File in locale
Per indicare i file in locale è necessario specificare il path completo. Usa:

```python
import os 
os.path.dirname(os.path.realpath(__file__))+os.sep+"nomefile"
```



## TODO
  - envfile



