#!/bin/bash

#Iteriamo tutte le cartelle creiamo i venv
for D in *; do
    if [ -d "${D}" ]; then
        echo "Processo batch >> ${D}";   
        if [ -f "${D}/requirements.txt" ]; then echo "Creazione venv"; python -m venv ./${D}/venv; ${D}/venv/bin/python -m pip install -r ./${D}/requirements.txt; fi
    fi
done

echo "Enviroment creati"