import sys
import mysql.connector


import logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger("Update Metro Distance")


def get_db_connector(host, user, password, port, db_name):
    '''
    Crea il db mysql connector
    :param host:
    :param user:
    :param password:
    :param port:
    :param db_name:
    :return:
    :rtype: MYSQLConnection
    '''
    logger.info("CREAZIONE DATABASE CONNECTOR => host: "+host)
    return mysql.connector.connect(
        host=host,
        user=user,
        passwd=password,
        port=port,
        database=db_name
    )

def get_null_metro_distance(connector, table_location, column_metro_distance):
    '''

    :param connector: db mysql connector
    :param table_location:
    :param column_metro_distance:
    :return:
    '''
    cursor = connector.cursor()
    query = ("SELECT id,name,latitude,longitude, address FROM "+table_location+" WHERE "+column_metro_distance+" IS NULL OR "+column_metro_distance+"='';")

    cursor.execute(query)

    resp = []
    for (id,name,latitude,longitude, address) in cursor:
        resp.append({'id':id, 'name':name, 'lat': latitude, 'long': longitude, 'address': address})

    cursor.close()
    return resp

def update_metro_distance(connector, table_location, column_metro_distance, id_location, distance_metro_value):
    '''
    Aggiorna la distanza dalla metro per una determinata location
    :param connector: db mysql connector
    :param table_location: tabella locations
    :param column_metro_distance:  colonna distanza metro
    :param id_location: id della location da aggiornare
    :param distance_metro_value: valore distanza location
    :return:
    '''
    cursor = connector.cursor()

    query_update_metro_distance = "UPDATE "+table_location+" SET "+column_metro_distance+"='"+distance_metro_value+ \
                                  "' WHERE id='"+id_location+"';"

    cursor.execute(query_update_metro_distance)
    connector.commit()
    cursor.close()




