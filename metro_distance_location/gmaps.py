import requests
import json
import os

def get_lista_metro_json():

    line_metro = []
    with open(os.path.dirname(os.path.realpath(__file__))+os.sep+'metro_line.json','r') as fp:
        line_metro = json.load(fp)

    return list(map(lambda x : {
                                "name" : x['metroLabel'].lower().replace('stazione di ',''),
                                "linea" : x['lineaLabel'].replace('linea ',''),
                                }, line_metro))


def get_metroline_by_name(nome_stazione, lista_metro):
    return list(map(lambda y: y['linea'],
                    filter(lambda x: x['name'].lower() == nome_stazione.lower(), lista_metro)))




def get_nearby(lat, long, api_key, rankby="distance", type_loc = None):
    '''
    Recupera luoghi vicini,
    :param lat: latitudine esempio 45.5118566
    :param log: longitudine
    :param type_loc: tipo di luogo da cercare, se vogliamo specificarlo
    :param api_key: key maps
    :param rankby: ordina per distanza
    :return:
    '''

    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=it&location='+lat+","+long
    if rankby is not None:
        url += '&rankby='+rankby
    if type_loc is not None:
        url +=  '&type='+type_loc

    if api_key is None:
        raise Exception("Inserire una chiave api per google maps Nearby places")
    else:
        url += '&key='+api_key


    r = requests.get(url, verify=False)
    if r.status_code == 200:
        risposta = r.json()
        if risposta['status'] == 'OK':
            return risposta
    return None


def get_distance_between(origin_lat, origin_long, dest_lat, dest_long, api_key, mode="walking"):
    '''
    Recupera distanza in km e minuti tra 2 punti
    :param origin_lat:
    :param origin_long:
    :param dest_lat:
    :param dest_long:
    :param api_key:
    :param mode: walking/driving
    :return:
    '''
    url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&language=it&origins='+origin_lat+','+origin_long
    url += '&destinations='+dest_lat+","+dest_long
    if mode is not None:
        url += '&mode='+mode

    if api_key is None:
        raise Exception("Inserire una chiave api per google maps Distance Metrix")
    else:
        url += '&key='+api_key

    r = requests.get(url, verify=False)
    if r.status_code == 200:
        risposta = r.json()
        if risposta['status'] == 'OK':
            return risposta
    return None


def get_nearest_metro(lat, long, api_key):
    '''
    Dato una coordinata recupera la metro piu vicina
    :param lat:
    :param long:
    :param api_key: chiave maps
    :return: una tupla (linea metro, stazione, distanza metri, distanza minuti a piedi)
    :rtype: tuple
    '''

    #controllo stringa
    lat=str(lat)
    long=str(long)

    resp_nearby = get_nearby(lat, long, api_key=api_key, type_loc='subway_station')
    if resp_nearby is not None and len(resp_nearby['results']) > 0:
        nearest_metro = resp_nearby['results'][0] #arrivano ordinati quindi il primo piu vicino
        name = nearest_metro['name']
        place_id = nearest_metro['place_id']
        lat_metro = str(nearest_metro['geometry']['location']['lat'])
        long_metro = str(nearest_metro['geometry']['location']['lng'])

        #ora recuperiamo la distanza
        resp_distance = get_distance_between(origin_lat=lat, origin_long=long,
                             dest_lat=lat_metro, dest_long=long_metro,
                             api_key=api_key)
        if resp_distance is not None and len(resp_distance['rows']) > 0 and resp_distance['rows'][0]['elements'][0]['status'] == 'OK':
            return (get_metroline_by_name(name, lista_metro=get_lista_metro_json() ),
                    name,
                    resp_distance['rows'][0]['elements'][0]['distance']['value'],
                    resp_distance['rows'][0]['elements'][0]['duration']['text']
                    )

    return None
