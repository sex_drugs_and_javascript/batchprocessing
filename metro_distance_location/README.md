
# Distance Locations from metro

Aggiorna la colonna **metroDistance** con la distanza della location dalla metro piu vicina.

Inserisce una riga del tipo: M1 - San Babila - 324 - 4 min

- la metro, possono essere piu di una separate da uno slash, ad esempio M1/M2
- la stazione della metro
- la distanza in metri dalla metro
- tempo di percorrenza a piedi dalla metro


## Lista metro di milano recuperate da wikidata

Recuperiamo tutte le metro di milano con la linea associata (linea M1, linea M2 ..)
```sparql
##QUERY WIKIDATA

SELECT ?metro ?metroLabel ?linea ?lineaLabel
WHERE
{
    ?metro wdt:P31 wd:Q928830 .
	?metro wdt:P127 wd:Q381955 .
    SERVICE wikibase:label {
      bd:serviceParam wikibase:language "it" .
    }  
    ?metro wdt:P81 ?linea .
 }
ORDER BY(?linea)
LIMIT 1000
```

risultato salvato in metro_line.json
