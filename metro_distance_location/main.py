import sys

from utils import getConfig
from gmaps import get_nearest_metro
from db import get_db_connector, get_null_metro_distance, update_metro_distance


import logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger("Update Metro Distance")


def main():
    logger.info("AVVIO Update Metro Distance")
    configurazione = getConfig()

    logger.debug("Caricata Configurazione")

    mysql_connector = get_db_connector(
        host=configurazione['MYSQL_HOST'],
        user=configurazione['MYSQL_USER'],
        port=configurazione['MYSQL_PORT'],
        password=configurazione['MYSQL_PASSWORD'],
        db_name=configurazione['MYSQL_DB'])

    logger.debug("Creato db Connector")

    for loc in get_null_metro_distance(connector=mysql_connector,
        table_location=configurazione['MYSQL_LOCATION_TABLE'],
        column_metro_distance=configurazione['MYSQL_COLUMN_NEAR_METRO']):

        logger.info("Esecuzione su: "+loc['name'])

        resp_nearest_metro = get_nearest_metro(lat=loc['lat'],
                                long=loc['long'],
                                api_key=configurazione['API_MAPS_KEY']
                                )

        logger.info("Trovato: " + str(resp_nearest_metro))

        if len(resp_nearest_metro[0]) > 0:
            nearest_value = f"{'/'.join(resp_nearest_metro[0])} - {resp_nearest_metro[1]} - {resp_nearest_metro[2]} - {resp_nearest_metro[3]}"

            update_metro_distance(connector=mysql_connector,
                                  table_location=configurazione['MYSQL_LOCATION_TABLE'],
                                  column_metro_distance=configurazione['MYSQL_COLUMN_NEAR_METRO'],
                                  id_location=loc['id'],
                                  distance_metro_value=nearest_value)
            logger.info("Aggiornato a db")

    mysql_connector.close()
    logger.debug("Chiuso db Connector")
    logger.info("Fine Update Metro Distance")

if __name__ == '__main__':
    main()