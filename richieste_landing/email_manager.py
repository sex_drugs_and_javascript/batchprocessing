#!/usr/bin/env python
# -*- coding: utf-8 -*-

############################## IMPORT ###############################
import sys, os
import imaplib
import email
import os
import re
import datetime
import pytz
import uuid
import time
import dateparser

import utils
import telegram
from db import get_db_connector
from mail import send_mail_conferma_richiesta_landing, send_mail_accredito, send_mail_conferma_creazione_choodle

############################## CLASS DEFINITION ##############################

import logging



class EmailManager:

	def __init__(self, config, out_emails):

		# Caricamento dei dati di configurazione
		self.config = config
		self.email_config = self.config.get('STUBBED')

		# Inizializzazione del Logger
		logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
		self.logger = logging.getLogger("RICHIESTE LANDING")

		# Variabili d'istanza
		self.imap_connection = ''
		self.out_emails = out_emails
		self.inbox_emails = list()

		self.mydb = get_db_connector(
			host=config['MYSQL_HOST'],
			user=config['MYSQL_USER'],
			port=config['MYSQL_PORT'],
			password=config['MYSQL_PASSWORD'],
			db_name=config['MYSQL_DB'])

		self.mycursor = self.mydb.cursor()

		print('Connected to DB: ', self.mydb)

		self.bot_token = self.config.get('BOT_TOKEN')
		self.bot_chatID = self.config.get('CHAT_ID')

############################## CORE FUNCTIONS ##############################


	def imap_login(self):
		try:
			# Provo a stabilire una connessione con il Mail Server tramite protocollo IMAP
			self.imap_connection = imaplib.IMAP4_SSL(self.email_config.get('IMAP_SERVER'))

			# Effettuo il login attraverso le credenziali
			self.imap_connection.login(self.email_config.get('EMAIL_ADDRESS'), self.email_config.get('EMAIL_PASSWORD'))

			self.logger.info("*** Connessione effettuata con {}".format(self.email_config.get('IMAP_SERVER')))
			self.logger.info("*** {}".format((datetime.datetime.today() + datetime.timedelta(minutes=120)).strftime("%b %d %Y %H:%M:%S")))

		except Exception as e:
			self.imap_connection = None
			self.logger.error("***")
			self.logger.error("*** >>> Connessione non riuscita con il server IMAP!")
			self.logger.error(str(e))



	def fetch_emails(self):
		'''
		Questa funzione deve:
		 - recuperare nuove mail arrivate alla landing
		 - segnare quelle che sono state recuperate
		 - rittornare le mail che verranno elaborate
		:return:
		'''

		try:

			# Seleziono la folder della mailbox da cui recuperare i messaggi
			count_mex = self.imap_connection.select(self.email_config.get('MAIN_FOLDER'))
			#print('Total number of messages found: ',count_mex)

			#datetime.timedelta(minutes=15)
			# Creo una variabile 'time_delta', che contiene la data di ieri nel formato "dd-Mese-2018"
			# UTC time - 2 ore (make it) - DELTA
			time_delta = (datetime.date.today() - datetime.timedelta(minutes=120)).strftime("%d-%b-%Y")

			# Uso il metodo 'search' per estrarre tutte le email dalla folder selezionata
			# Sfruttando la variabile 'time_delta' riesco ad estrarre solo i messaggi inviati da ieri ad oggi, non i precedenti
			# 'search' restituisce una tupla, ma 'status' non ha un valore informativo utile
			# In 'data', invece, troverò tutto ciò che mi serve!
			status, data = self.imap_connection.search(None, '(SENTSINCE {})'.format(time_delta))

			#print('Last day email: ', status, data)
			# Preparo la lista da restituire al chiamante: conterrà i messaggi estratti!
			email_bucket = list()


			#Recuperiamo ultimo uid processato
			last_uid = self.get_last_uid()

			# IMAP assegna ad ogni messaggio all'interno di una speicfica folder un identificativo univoco
			# Tale 'uid' è utile per effettuare tutte le operazioni IMAP sul singolo messaggio!
			# All'interno di 'data[0]' trovo tutti gli uid, quindi richiamo un ciclo per estrarre tutti i messaggi
			# parsiamo solo quelli maggiori di get last uid
			print(data[0].split())
			print(last_uid)
			for uid in filter(lambda x: int(x) > last_uid, data[0].split()):
				try:
					# Richiamo il metodo 'fetch' per estrarre dalla folder il messaggio associato all'uid dell'iterazione in corso
					status, email_data = self.imap_connection.fetch(uid, '(RFC822)')
					#print('Email: ', uid, status, data)
					# Richiamo 'message_from_bytes' per istanziare 'msg', che è l'oggetto di tipo Message che contiene tutte le informazioni
					# della specifica mail in analisi
					msg = email.message_from_bytes(email_data[0][1])
					#print(msg)

					received_date = email.header.decode_header(msg['Date'])[0][0]
					#print(received_date, type(received_date))

					date_time_received_date = datetime.datetime.strptime(received_date, "%a, %d %b %Y %H:%M:%S %z").replace(tzinfo=None)

					#Check if there is a request in the last 15 minutes
					#if (date_time_received_date + datetime.timedelta(minutes=int(self.config.get('MAIL_INTERVAL_TIME')))) > datetime.datetime.now():

						# Estraggo l'oggetto dell'email
					subj = email.header.decode_header(msg['Subject'])[0][0]

					#print('subject ', subj)

					if subj == 'OFFERTA CHEPLAN': #TODO: funzioni separate

						#print(datetime.datetime.now(), date_time_received_date)
						#print(date_time_received_date)
						#sys.exit(0)

						msg = str(msg.get_payload().encode('utf-8')).replace('\\n','').replace('\\r','').replace('=','')
						#print('Email: ', msg)

						my_id = str(uuid.uuid4()) #random TODO: da risolvere
						#print(type(my_id),my_id )

						#print('NOME :', msg.split(self.config.get('LABEL_NAME'))[1].split('<br>')[0])
						name = str(msg.split(self.config.get('LABEL_NAME'))[1].split('<br>')[0]).strip() #.replace(" ","")

						if self.config.get('LABEL_TELEFONO') in msg:
							telephone = str(msg.split(self.config.get('LABEL_TELEFONO'))[1].split('<br>')[0]).replace(" ","")
						else:
							telephone = None

						#print('LABEL_PRIVACY :', msg.split(self.config.get('LABEL_PRIVACY'))[1].split('<br>')[0])

						if self.config.get('LABEL_PRIVACY') in msg:
							privacy = str(msg.split(self.config.get('LABEL_PRIVACY'))[1].split('<br>')[0]).replace(" ","")
						else:
							privacy = None

						if privacy == 'on':
							privacy = 1
						elif privacy != 'on' and privacy != None:
							privacy = 0

						#print('LABEL_DATE :', msg.split(self.config.get('LABEL_DATE'))[1].split('<br>')[0])
						date = dateparser.parse(msg.lower().split(self.config.get('LABEL_DATE'))[1].split('<br>')[0].replace(".","/").replace(" ","").replace(":","")) #,  settings={'TO_TIMEZONE': 'UTC'} )

						#print('LABEL_EMAIL :', msg.split(self.config.get('LABEL_EMAIL'))[1].split('<br>')[0])
						mail = str(msg.split(self.config.get('LABEL_EMAIL'))[1].split('<br>')[0]).replace(" ","")

						mail_object = {"id": my_id,
									   "name": name,
									   "telephone": telephone,
									   "privacy": privacy,
									   "mail": mail,
									   "date": date,
									   "uid": uid,
									   "subject": subj}

						# Aggiungo il dizionario 'fetched_email' al bucket
						#email_bucket.append((my_id,name,telephone,privacy,date,mail,uid))
					elif subj == "ACCREDITO WEDDING CS" or subj == "ACCREDITO ELISA 18":
						#ACCREDITO WEDDING CS
						msg = str(msg.get_payload().encode('utf-8')).replace('\\n', '').replace('\\r', '').replace('=', '')

						LABEL_NAME="Nome:"
						name = str(msg.split(LABEL_NAME)[1].split('<br>')[0]).strip()
						LABEL_SURNAME = "Cognome:"
						surname = str(msg.split(LABEL_SURNAME)[1].split('<br>')[0]).strip()
						LABEL_DATE = "data di nascita:"
						try:
							print(msg.lower())
							birth_date = dateparser.parse(msg.lower().split(LABEL_DATE)[1].split('<br>')[0].replace(".","/").replace(" ","").replace(":",""))#,  settings={'TO_TIMEZONE': 'UTC'} )
						except Exception as e:
							self.logger.error(e)
							birth_date = msg.lower().split(LABEL_DATE)[1].split('<br>')[0]

						LABEL_EMAIL = "Email:"
						mail = str(msg.split(LABEL_EMAIL)[1].split('<br>')[0]).replace(" ", "").strip()
						LABEL_PRIVACY = "Privacy:"

						if LABEL_PRIVACY in msg:
							privacy = str(msg.split(self.config.get('LABEL_PRIVACY'))[1].split('<br>')[0]).replace(" ","")
						else:
							privacy = None

						if privacy == 'on':
							privacy = 1
						elif privacy != 'on' and privacy != None:
							privacy = 0

						mail_object = {'id': str(uuid.uuid4()),
									   "nome": name,
									   "cognome": surname,
									   "data_nascita": birth_date,
									   "mail": mail,
									   "privacy": privacy,
									   "uid": uid,
									   "subject": subj}
					elif subj == "CREAZIONE CHOODLE":

						#msg = str(msg.get_payload().encode('utf-8')).replace('\\n', '').replace('\\r', '').replace('=', '')
						#print(type(msg), msg)

						mail = str(str(msg).split('From: ')[1].split('<')[1].split('>')[0].strip())

						for part in msg.walk():
							# each part is a either non-multipart, or another multipart message
							# that contains further parts... Message is organized like a tree
							#print(part.get_content_type(), '***************************')
							if part.get_content_type() == 'text/plain':
								#print(part.get_payload(None, True).decode('utf-8'), type(part.get_payload(None, True).decode('utf-8')))
								msg = part.get_payload(None, True).decode('utf-8').replace('\n', '<br>').replace('\r', ' ')
								break
								#print(part.get_payload(None, True)) # prints the raw text

						#msg = msg.get_payload(None, True).decode('utf-8').replace('\\n', '<br>').replace('\\r', ' ')
						#print('Choodle: ', msg)

						LABEL_NAME = "nome:"
						name = str(msg.split(LABEL_NAME)[1].split('<br>')[0]).strip()
						LABEL_LOCATION = "location:"
						location = str(msg.split(LABEL_LOCATION)[1].split('<br>')[0]).strip()
						LABEL_DATE = "data festa:"

						try:
							#print(msg.lower().split(LABEL_DATE)[1].split('<br>')[0].replace(".", "/").replace(" ","").replace("-"," ")[:16])
							#print('OOOOOOK')
							party_date = dateparser.parse(msg.lower().split(LABEL_DATE)[1].split('<br>')[0].replace(".", "/").replace(" ","").replace("-"," ")[:16])
							party_date = party_date.astimezone(pytz.timezone(self.config.get('selected_timezone')))
							print('Parsed date', party_date)
						except Exception as e:
							self.logger.error(e)
							print('Error: ', e)
							party_date = msg.lower().split(LABEL_DATE)[1].split('<br>')[0]

						#print(party_date,msg.lower().split(LABEL_DATE)[1].split('<br>')[0].replace(".", "/").replace(" ","").replace(":","")[:10])

						DESCRIPTION = "descrizione:"
						descrizione = str(msg.split(DESCRIPTION)[1].split('<br>')[0]).strip()

						mail_object = {'id': str(uuid.uuid4()),
									   "nome": name,
									   "location": location,
									   "data_festa": party_date,
									   "descrizione": descrizione,
									   "mail": mail,
									   "uid": uid,
									   "subject": subj}

						print(mail_object)

					else:
						#print(subj)
						mail_object = {'subject': subj}


					email_bucket.append(mail_object)
				except Exception as e:
					self.logger.error("***")
					self.logger.error("*** >>> Errore durante il PARSING della mail")
					self.logger.error("Errore: " + str(e))
					exc_type, exc_obj, exc_tb = sys.exc_info()
					fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
					self.logger.error(exc_type, fname, exc_tb.tb_lineno)
					continue #jump problematic email

			# Restituisco al chiamante l'intera lista di email estratte nella finestra temporale considerata
			return email_bucket

		except Exception as e:
			self.logger.error("***")
			self.logger.error("*** >>> Errore durante il processo di estrazione delle email dal folder INBOX!")
			self.logger.error("Errore: " + str(e))
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			self.logger.error(exc_type, fname, exc_tb.tb_lineno)
			return []



	def save_fetched_emails(self, inbox_emails):

		self.logger.info('Found emails to insert in DB ' + str(len(inbox_emails)))

		try:
			#TODO: create tuple da inserire
			to_insert_emails=[]
			for mail in inbox_emails:

				#(my_id,name,telephone,privacy,date,mail,uid, subject)

				if mail['subject'] == 'OFFERTA CHEPLAN':
					to_insert_emails.append((mail['id'], mail['name'], mail['telephone'], mail['privacy'], mail['date'], mail['mail'], mail['uid'], mail['subject'], datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone'))), datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone')))))

				elif mail['subject'] == 'ACCREDITO WEDDING CS' or  mail['subject'] == 'ACCREDITO ELISA 18':
					'''
					
					mail_object = {'id': str(uuid.uuid4()),
								   "nome": name,
								   "cognome": surname,
								   "data_nascita": birth_date,
								   "mail": mail,
								   "privacy": privacy,
								   "uid": uid,
								   "subject": subj}
					'''

					to_insert_emails.append((mail['id'],
											 mail['nome']+" "+mail['cognome'],
											 "-",
											 mail['privacy'],
											 mail['data_nascita'],
											 mail['mail'],
											 mail['uid'],
											 mail['subject'],
											 datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone'))),
											 datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone')))))

				elif mail['subject'] == 'CREAZIONE CHOODLE':

					print('Updated richieste counter')
					'''
					mail_object = {'id': str(uuid.uuid4()),
								   "nome": name,
								   "location": location,
								   "data_festa": birth_date,
								   "descrizione": descrizione,
								   "mail": mail,
								   "uid": uid,
								   "subject": subj}
					'''

					to_insert_emails.append((mail['id'],
											 mail['nome'],
											 "-",
											 0,
											 mail['data_festa'],
											 mail['mail'],
											 mail['uid'],
											 mail['subject'],
											 datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone'))),
											 datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone')))))

			sql = "INSERT INTO richiesta_landings (id,name,telephone,privacy,date,mail,email_count, subject, createdAt, updatedAt) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

			self.mycursor.executemany(sql, to_insert_emails)

			self.mydb.commit()

			print(self.mycursor.rowcount, "was inserted in richieste landings")

		except Exception as e:
			self.logger.error("ERRORE SCRITTURA DB")
			self.logger.error(e)


	def get_last_uid(self):
		'''
		Recupera l'ultimo uid inserito
		:return:
		'''
		self.logger.info("Recuperiamo ultimo uid processato")

		try:
			sql = "SELECT MAX(email_count) as m FROM richiesta_landings"
			self.mycursor.execute(sql)

			valore = [float("Inf")]
			for (m) in self.mycursor:
				valore = m
			return valore[0]
		except Exception as e:
			self.logger.error("Errore recupero ultimo uid")
			self.logger.error(e)
			return float("Inf")

############################## UTILITY FUNCTIONS ##############################
	def move_email(self, uid, origin_folder, destination_folder):

		try:
			# Sposto il focus sulla folder di origine
			self.imap_connection.select(origin_folder)

			# Copio il messaggio dalla folder di orgine a quella di destinazione
			self.imap_connection.copy(uid, destination_folder)

			# Applico il flag 'Deleted' al messaggio in analisi
			self.imap_connection.store(uid, '+FLAGS', '(\\Deleted)')

			# Con il metodo 'expunge' sposto nel cestino tutte le email marcate 'Deleted'
			self.imap_connection.expunge()

		except Exception as e:
			self.logger.info("***")
			self.logger.error("*** >>> Si è verificato un errore durante lo spostamento di un'email nella folder CHATBOT!")
			self.logger.error(str(e))




	def extract_body(self, msg):

		# La prima diramazione è una chiamata ricorsiva per raggiungere il body effettivo dell'email
		if msg.is_multipart():
			return self.extract_body(msg.get_payload(0))

		# La seconda diramazione è relativa al caso non 'multipart', in cui posso chiamare direttamente
		# 'get_payload' e restituire il body al chiamante
		# Di regola dovrei decodificare 'latin1' nella funzione chiamante, ma lo faccio qui per facilitare
		# la sostituzione degli 'a capo' con il placeholder preferito di Cinzia!
		else:
			body = msg.get_payload(None, True).decode('latin1').replace('\r\n',' ###N ')
			return body



	def preprocess_body(self, body):
		
		# Questa funzione è utile per pulire il body da componenti non utili al Bot
		# Va perfezionata perché fa pena

		tmp =  body.split('___')[0]
		tmp = ' '.join(tmp.splitlines()).strip()

		tmp = tmp.split('**Da:**')[0]
		tmp = ' '.join(tmp.splitlines()).strip()

		tmp = tmp.split('**![unnamed]')[0]
		tmp = ' '.join(tmp.splitlines()).strip()

		tmp = tmp.split('**From:**')[0]
		tmp = ' '.join(tmp.splitlines()).strip()

		# Sostituisco il placeholder preferito di Cinzia con '\n' in modo che
		# il body non arrivi su una riga sola, ma mantenga gli 'a capo' previsti dal messaggio originale
		tmp = re.sub('###N','\n',tmp)

		return tmp



	def fetch_attachment(self, msg):

		# Funzione per l'estrazione dell'allegato di un'email
		try:

			for part in msg.walk():

				if part.get_content_maintype() == 'multipart':
					continue

				if part.get('Content-Disposition') is None:
					continue

				if bool(part.get_filename()):

					# Controllo l'estensione del file allegato
					# Solo 'xlsx' e 'xls' sono ammessi!
					if part.get_filename().endswith('.xlsx') or part.get_filename().endswith('xls'):
						return part.get_payload(decode = True)

			# Se il return precedente non viene attivato, restituisco 'NULL'
			# ad indicare che non ho trovato allegati
			return self.config.get('STATUS_NULL')

		except Exception as e:
			self.logger.error("***")
			self.logger.error("*** >>> Si è verificato un errore nell\'estrazione di un allegato.")
			self.logger.info("*** >>> Errore: " + str(e))
			return self.config.get('STATUS_NULL')



	def fetch_file_name(self, msg):

		# Funzione per l'estrazione del nome del file allegato di un'email
		try:
			for part in msg.walk():

				if part.get_content_maintype() == 'multipart':
					continue

				if part.get('Content-Disposition') is None:
					continue

				if bool(part.get_filename()):
					if part.get_filename().endswith('.xlsx') or part.get_filename().endswith('xls'):
						return part.get_filename()

			return self.config.get('STATUS_NULL')

		except Exception as e:
			self.logger.error("***")
			self.logger.info("*** >>> Si è verificato un errore nell\'estrazione di un allegato.")
			self.logger.info("*** >>> Errore: " + str(e))
			return self.config.get('STATUS_NULL')




############################## MAIN FUNCTIONS ##############################


	def run(self):

		# Effettuo il login con IMAP sulla casella di posta
		self.imap_login()

		if self.imap_connection is not None:

			# Recupero tutte le email ricevute nella finestra temporale considerata
			inbox_emails = self.fetch_emails()

			if len(inbox_emails) > 0:

				# Processo tutte le email estratte: le invio al bot e gestisco le risposte ricevute
				self.save_fetched_emails(inbox_emails)

				# tupla ---> (id,name,telephone,privacy,date,mail)

				#telegram.telegram_bot_sendtext(
				#	bot_chatID=self.bot_chatID,
				#	bot_token=self.bot_token,
				#	bot_message='Richieste Landing Report del '+ (datetime.datetime.today() + datetime.timedelta(minutes=120)).strftime("%b %d %Y %H:%M:%S"))


				#message = ''
				for index, mail_object in enumerate(inbox_emails):

					if mail_object['subject'] == 'OFFERTA CHEPLAN':
						#(my_id, name, telephone, privacy, date, mail, uid, subject)
						if mail_object['date'] != None:
							message = 'Nuova Richiesta ' + str(index) + '\n' +\
							 		'Nome: ' + mail_object['name'] + '\n' + 'Telefono: ' + str(mail_object['telephone'])  + '\n' \
						     		+ 'Privacy: ' + str(mail_object['privacy'])  + '\n' + 'Date: ' + mail_object['date'].strftime("%d-%b-%Y")  + '\n' \
									+ 'Mail: ' + mail_object['mail'] + '\n'
						else:
							message = 'Nuova Richiesta ' + str(index) + '\n' +\
							'Nome: ' + mail_object['name'] + '\n' + 'Telefono: ' + str(mail_object['telephone'])  + '\n' \
							+ 'Privacy: ' + str(mail_object['privacy'])  + '\n' + 'Date: -' + '\n' + 'Mail: ' + mail_object['mail'] + '\n'
						#print('**********************')
						#print(message)
						telegram.telegram_bot_sendtext(
							bot_chatID=self.bot_chatID,
							bot_token=self.bot_token,
							bot_message=message)

						### invio mail conferma
						send_mail_conferma_richiesta_landing(nome=mail_object['name'],
															 mail=mail_object['mail'],
															 config=self.config)
					elif mail_object['subject'] == 'ACCREDITO WEDDING CS':
						'''

						mail_object = {'id': str(uuid.uuid4()),
									   "nome": name,
									   "cognome": surname,
									   "data_nascita": birth_date,
									   "mail": mail,
									   "privacy": privacy,
									   "uid": uid,
									   "subject": subj}
						'''


						link_calendar = "https://collaudoserver.cheplan.it/api/calendar/downloadCalendar/w1"
						send_mail_accredito(mail=mail_object['mail'],config=self.config,
											link_calendario=link_calendar,
											data_evento="Mercoledì 26 Giugno 2019",
											evento_di="Clari & Santiago",
											luogo="Just Cavalli Milano - Torre Branca, Via Luigi Camoens, 20121",
											chiedi_di="Clari Cheplan",
											subject="Conferma partecipazione all'evento di Clari & Santiago"
											)

					elif mail_object['subject'] == 'ACCREDITO ELISA 18':

						#print(mail_object)

						link_calendar = "https://collaudoserver.cheplan.it/api/calendar/downloadCalendar/elisa18"
						send_mail_accredito(mail=mail_object['mail'],config=self.config,
											link_calendario=link_calendar,
											data_evento="Mercoledì 26 Giugno 2019",
											evento_di="Festa +18 Elisa",
											luogo="Kitsch - C.so Sempione, 5",
											chiedi_di="Elisa Cheplan",
											subject="Conferma partecipazione alla festa +18 di Elisa"
											)

					elif mail_object['subject'] == 'CREAZIONE CHOODLE':

						'''
						mail_object = {'id': str(uuid.uuid4()),
									   "nome": name,
									   "location": location,
									   "data_festa": birth_date,
									   "descrizione": descrizione,
									   "mail": mail,
									   "uid": uid,
									   "subject": subj}
						'''

						choodle_fields = [(mail_object['id'], mail_object['nome'], mail_object['location'], mail_object['data_festa'], mail_object['descrizione'], datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone'))), datetime.datetime.now(pytz.timezone(self.config.get('selected_timezone'))))]

						print(choodle_fields)
						sql = "INSERT INTO choodles (id,name,location, date, summary, createdAt, updatedAt) VALUES (%s, %s, %s, %s, %s, %s, %s)"

						self.mycursor.executemany(sql, choodle_fields)

						self.mydb.commit()

						print(self.mycursor.rowcount, "was inserted.")
						print('')
						print(self.mycursor)

						choodle_link = self.config['choodle_address'] + mail_object['id']

						#sys.exit(0)
						### invio mail conferma
						send_mail_conferma_creazione_choodle(mail=mail_object['mail'],
															 link = choodle_link,
															 config=self.config)



			else:
				self.logger.info('Nessuna nuova richiesta trovata')

		else:
			self.logger.info("\n\n\n*** ERRORE NELLA CONNESSIONE IMAP ***\n")


def main():

	# Carico i dati di configurazione
	config = utils.getConfig()

	# Inizializzo la lista che userò per tenere traccia delle email 'out of scope'
	out_emails = []

	start_time = time.time()
	time_array = list()
	time_array.append(start_time)

	# Inizializzazione dell'EmailManager
	em = EmailManager(config, out_emails)

	# Lancio il ciclo di elaborazione dell'EmailManager
	em.run()

	end_time = time.time()

	print("Elaboration time: ", end_time - start_time)

#####################################################################################################

if __name__ == '__main__':
	main()
	