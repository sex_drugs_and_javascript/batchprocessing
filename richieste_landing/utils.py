import yaml
import os

def getConfig(path='config.yml'):
    path = os.path.dirname(os.path.realpath(__file__))+os.sep+path
    stream = open(path, "r")
    configurazione = yaml.load(stream)

    #sostituisce la key con la variabile d'ambiente
    conf_parsed = {}
    for key,val in configurazione.items():
        conf_parsed[key] = os.getenv('key', val)
    return conf_parsed

def readMailTemplate(template_name):
    path = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'mail_template' + os.sep + template_name
    template = ""
    with open(path, "r", encoding='utf-8') as f:
        template = f.read()
    return template
