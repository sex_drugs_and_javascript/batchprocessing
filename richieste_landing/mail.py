# invio mail tramite mailgun
import requests

from utils import readMailTemplate

def send_mail(domain, api_key, from_mail, to_mail, subject, mail_body):
    r = requests.post(
        "https://api.eu.mailgun.net/v3/"+domain+"/messages",
        auth=("api", api_key),
        data={"from": from_mail,
              "to": [to_mail],
              "subject": subject,
              "html": mail_body})
    print(r)
    print(r.text)

def send_mail_conferma_richiesta_landing(mail, config, nome):
    mailFile = readMailTemplate('conferma_landing.html')
    mailFile = mailFile.replace("###NOME###", nome)
    send_mail(domain=config['MAILGUN_DOMAIN'],
              api_key=config['MAILGUN_API_KEY'],
              from_mail=config['MAIL_FROM'],
              to_mail=mail,
              subject="Conferma richiesta preventivo cheplan",
              mail_body=mailFile)

def send_mail_accredito(mail, config, link_calendario, data_evento, evento_di, luogo, chiedi_di, subject ):
    mailFile = readMailTemplate('accredito.html')
    mailFile = mailFile.replace("###LINK_CALENDARIO###", link_calendario)
    mailFile = mailFile.replace("###DATA_EVENTO###", data_evento)
    mailFile = mailFile.replace("###EVENTO_DI###", evento_di)
    mailFile = mailFile.replace("###LUOGO_EVENTO###", luogo)
    mailFile = mailFile.replace("###CHIEDI_DI###", chiedi_di)

    send_mail(domain=config['MAILGUN_DOMAIN'],
              api_key=config['MAILGUN_API_KEY'],
              from_mail=config['MAIL_FROM'],
              to_mail=mail,
              subject=subject,
              mail_body=mailFile)

def send_mail_conferma_creazione_choodle(mail,link,config):
    mailFile = readMailTemplate('creazione_choodle.html')
    mailFile = mailFile.replace("###LINK###", link)
    send_mail(domain=config['MAILGUN_DOMAIN'],
              api_key=config['MAILGUN_API_KEY'],
              from_mail=config['MAIL_FROM'],
              to_mail=mail,
              subject="Conferma creazione CHOODLE",
              mail_body=mailFile)
