import sys
import mysql.connector


import logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger("RICHIESTE LANDING")


def get_db_connector(host, user, password, port, db_name):
    '''
    Crea il db mysql connector
    :param host:
    :param user:
    :param password:
    :param port:
    :param db_name:
    :return:
    :rtype: MYSQLConnection
    '''
    logger.info("CREAZIONE DATABASE CONNECTOR => host: "+host)
    return mysql.connector.connect(
        host=host,
        user=user,
        passwd=password,
        port=port,
        database=db_name
    )