import requests
import logging
import sys

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger("EXAMPLE")

if __name__ == "__main__":
	logger.info("AVVIO RICHIESTA a cheplan.it")
	r = requests.get('https://www.cheplan.it')
	if r.status_code == 200:
		logger.info("CHEPLAN RISPONDE CORRETTAMENTE!!")
	else:
		logger.error("CHEPLAN NON RISPONDE!!!!!!!!!!!!!!")


