import os
import logging
import sys

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger("EXAMPL2E")

if __name__ == "__main__":
	logger.info("AVVIO EXAMPLE2")
	env_var = os.getenv("EXAMPLE2", "non trovata")
	logger.info("IL VALORE DI EXAMPLE2 => "+env_var)
	print("CIAUX")