FROM python:3.7.3-alpine3.9

##installazione supercronic
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.9/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=5ddf8ea26b56d4a7ff6faecdd8966610d5cb9d85

RUN apk add --update --no-cache ca-certificates curl \
 && curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

RUN apk update \
    && apk add --virtual build-dependencies \
        build-base \
        gcc

#AGGIUNGIAMO TIMEZONE
#RUN apk add tzdata
#RUN cp /usr/share/zoneinfo/Europe/Brussels /etc/localtime
#RUN echo "Europe/London" >  /etc/timezone


# Copy crontab file in the cron directory
COPY ./crontab /etc/crontab
RUN chmod 0644 /etc/crontab
RUN touch /tmp/log

# aggiorniamo pip
RUN pip install --upgrade pip

RUN mkdir -p /batch
ADD . /batch
WORKDIR /batch

# creazione ambienti batch
RUN chmod a+x create_batch.sh && /bin/sh create_batch.sh


ENTRYPOINT ["supercronic"]
CMD ["/etc/crontab"]
